import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ClientApp from './Layouts/ClientApp'

export default function App() {
  return (
    <ClientApp></ClientApp>
  );
}
