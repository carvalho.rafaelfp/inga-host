import React  from 'react';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Title from '../Components/Ui/Title'

// const HostCard = ({ urlImage, title, price, capacity, avaliation }) => {

const HostCard = () => {
  return (
    <View style={styles.hostCard}>
      {/* <Image 
        style={styles.image}
        uri={}
      /> */}
      <View style={styles.mockImage}>
      </View>

      <View style={styles.hostDetails}>
        <View style={styles.leftContainer}>
          <Text style={styles.cardTitle}>
            Hotel Elo - Quarto quadruplo
          </Text>
          <View style={styles.capacityContainer}>
            <View style={styles.iconsContainer}>
              <Icon
                name="person"
                size={26}
                accessibilityLabel
                color="#808080"
              />

              <Icon
                name="person"
                size={26}
                accessibilityLabel
                color="#808080"
              />

              <Icon
                name="person"
                size={26}
                accessibilityLabel
                color="#808080"
              />

              <Icon
                name="person"
                size={26}
                accessibilityLabel
                color="#808080"
              />
            </View>
            <Text style={styles.capacityTitle}>
              Capacidade de hóspedes
            </Text>
          </View>
        </View>

        <View style={styles.rightContainer}>
          <Text style={styles.cardPrice}>
            R$ 145
          </Text>
          <Text style={styles.infoPrice}>
            por diária
          </Text>
        </View>
      </View>
        
    </View>
  );
};

export default HostCard;

const styles = StyleSheet.create({
  hostCard: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: 'auto',
    marginTop: '24px',
    borderRadius: '10px',

  },

  // image: {
  //   borderTopEndRadius: '10px',
  //   borderWidth: '1px',
  //   borderColor: '#C0C0C0',
  //   width: '100%',
  //   display: 'flex',
  //   flexDirection: 'row',
  //   padding: '6px',
  //   backgroundColor: '#FFF',
  // },

  mockImage: {
    display: 'flex',
    flexDirection: 'row',
    borderTopStartRadius: '10px',
    borderTopEndRadius: '10px',
    width: '100%',
    height: '120px',
    backgroundColor: '#000999'
  },

  hostDetails: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomStartRadius: '10px',
    borderBottomEndRadius: '10px',
    width: '100%',
    height: '80px',
    backgroundColor: '#EFF2F7',
  },

  leftContainer: {
    display: 'flex',
    flex: 2,
    flexDirection: 'column',
    padding: '10px',
  },

  cardTitle: {
    display: 'flex',
    flexDirection: 'row',
    fontSize: '14px',
    fontWeight: '500',
    color: '#808080',
  },

  rightContainer: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    padding: '10px',
  },

  cardPrice: {
    display: 'flex',
    flexDirection: 'row',
    fontSize: '22px',
    fontWeight: '600',
    color: '#808080',
  },

  capacityContainer: {
    display: 'flex',
    flexDirection: 'column',
    fontSize: '22px',
    fontWeight: '600',
    color: '#808080',
  },

  iconsContainer: {
    display: 'flex',
    flexDirection: 'row',
    fontSize: '22px',
    fontWeight: '600',
    color: '#808080',
  },

  capacityTitle: {
    display: 'flex',
    flexDirection: 'row',
    fontSize: '10px',
    fontWeight: '500',
    color: '#808080',
  },

  infoPrice: {
    display: 'flex',
    flexDirection: 'row',
    fontSize: '10px',
    color: '#808080',
  },
});