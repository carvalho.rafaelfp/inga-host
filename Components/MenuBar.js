import React  from 'react';
import { StyleSheet, Text, View } from 'react-native';
import HomeButton from '../Components/Ui/HomeButton';
import BookingsButton from './Ui/BookingsButton'
import AccountButton from './Ui/AccountButton'

const MenuBar = () => {
  return (
    <View style={styles.container}>
      <HomeButton/>
      <BookingsButton/>
      <AccountButton/>
    </View>
  );
};

export default MenuBar;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    height: '60px',
    padding: '25px',
    backgroundColor: '#DCDCDC',
  },
});