import React  from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const SearchBar = () => {
  const [text, onChangeText] = React.useState(null);

  return (
    <View style={styles.container}>
      <View style={styles.inputBorderRadius}>
        <TextInput
          style={styles.input}
          onChangeText={onChangeText}
          value={text}
          placeholder="Encontre sua acomodação"
        />
        <TouchableOpacity>
          <Icon
            name="search"
            size={26}
            accessibilityLabel
            color="#808080"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SearchBar;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 'auto',
    padding: '10px',
    backgroundColor: '#FFFFFF',
  },

  input: {
    padding: '0px',
    width: '100%',
    margin: '6px',
  },

  inputBorderRadius: {
    borderRadius: '6px',
    borderWidth: '1px',
    borderColor: '#C0C0C0',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    padding: '6px',
    backgroundColor: '#FFF',
  }
});