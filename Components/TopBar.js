import React  from 'react';
import { StyleSheet, View } from 'react-native';
import Logo from './Ui/Logo';

const TopBar = () => {
  return (
    <View style={styles.container}>
      <Logo/>
    </View>
  );
};

export default TopBar;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '60px',
    backgroundColor: '#C0c0c0c0',
  },
});