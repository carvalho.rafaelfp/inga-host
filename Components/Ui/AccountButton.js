import React  from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const AccountButton = () => {
  return (
    <TouchableOpacity style={styles.accountButton}>
      <Icon
        name="person"
        size={26}
        accessibilityLabel
         color="#808080"
      />
      <Text style={styles.label}>
        Conta
      </Text>
    </TouchableOpacity>
  );
};

export default AccountButton;

const styles = StyleSheet.create({
  accountButton: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },

  label: {
    fontSize: '10px',
    fontWeight: '600',
    color: '#808080',
  }
});