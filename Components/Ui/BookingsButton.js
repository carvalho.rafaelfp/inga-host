import React  from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const BookingsButton = () => {
  return (
    <TouchableOpacity style={styles.bookingsButton}>
      <Icon
        name="event"
        size={26}
        accessibilityLabel
         color="#808080"
      />
      <Text style={styles.label}>
        Reservas
      </Text>
    </TouchableOpacity>
  );
};

export default BookingsButton;

const styles = StyleSheet.create({
  bookingsButton: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },

  label: {
    fontSize: '10px',
    fontWeight: '600',
    color: '#808080',
  }
});