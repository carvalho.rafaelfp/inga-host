import React  from 'react';
import { TouchableOpacity, StyleSheet, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const HomeButton = () => {
  return (
    <TouchableOpacity style={styles.homeButton}>
      <Icon
        name="home"
        size={26}
        accessibilityLabel
         color="#808080"
      />
      <Text style={styles.label}>
        Home
      </Text>  
    </TouchableOpacity>
  );
};

export default HomeButton;

const styles = StyleSheet.create({
  homeButton: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },

  label: {
    fontSize: '10px',
    fontWeight: '600',
    color: '#808080',
  }
});