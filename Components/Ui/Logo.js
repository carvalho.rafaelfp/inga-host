import React  from 'react';
import { TouchableOpacity ,StyleSheet, Text } from 'react-native';

const Logo = () => {
  return (
    <TouchableOpacity>
      <Text style={styles.logo}>
        IngáHost  
      </Text>
    </TouchableOpacity>
  );
};

export default Logo;

const styles = StyleSheet.create({
  logo: {
    fontSize: '26px',
    fontWeight: 'bold',
    color: '#ffffff',
  },
});