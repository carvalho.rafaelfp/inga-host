import React  from 'react';
import { StyleSheet, Text } from 'react-native';

const Title = ({ label }) => {
  return (
    <Text style={styles.title}>
      {label}
    </Text>
  );
};

export default Title;

const styles = StyleSheet.create({
  title: {
    fontSize: '16px',
    fontWeight: '600',
    color: '#808080',
  },
});