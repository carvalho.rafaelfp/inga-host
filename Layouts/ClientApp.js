import React  from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import TopBar from '../Components/TopBar';
import MenuBar from '../Components/MenuBar'
import SearchBar from '../Components/SearchBar'
import Home from '../Pages/Home'


const ClientApp = () => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TopBar/>
        <SearchBar/>
      </View>
      
      <ScrollView style={styles.content}>
        <Home/>
      </ScrollView>

      <View style={styles.footer}>
        <MenuBar/>
      </View>
    </View>
  );
};

export default ClientApp;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexdirection: 'column',
    width: '100%',
    backgroundColor: '#fff000',
  },

  header: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: 'auto',
    backgroundColor: '#d8d8',
  },

  content: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    // height: '100%',
    padding: '15px',
    backgroundColor: '#ffffff',
  },

  footer: {
    position: 'fixed',
    bottom: '0',
    width: '100%',
    height: 'auto',
  },
});