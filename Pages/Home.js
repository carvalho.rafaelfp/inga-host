import React  from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Title from '../Components/Ui/Title';
import HostCard from '../Components/HostCard'

const Home = () => {
  return (
    <View style={styles.homeContainer}>
      <View style={styles.titleContainer}>
        <Title
          label="Listagem das acomodações" 
        />
      </View>
      
      
      <HostCard/>

      <HostCard/>

      {/* <View>
        {hosts.each(host => {
          <HostCard
            urlImage={host.urlImage}
            title={host.title}
            price={host.price}
            capacity={host.capacity}
            avaliation={host.avaliation}
          />
        })}
      </View> */}

    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  homeContainer: {
    display: 'flex',
    flexdirection: 'row',
    width: '100%',
    height: 'auto',
    marginBottom: '80px',
  },

  titleContainer: {
    marginStart: '6px',
  }
});